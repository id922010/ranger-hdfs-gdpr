'''
Created on 20 mai 2018
This scripts creates linux groups required for the matching with Atlas tag based policies.
It also assigns HDP Sandbox default users (like maria_dev, raj_ops) to those linux
groups.

On HDP Sandbox, we simulate the production cluster, hence all groups start with usr_prd_hdp_. One
can update the GROUP_PREFIX global variable to reflect another cluster deployment.

cmd:
  - python prepare_linux_env.py

parameter
- --submit, optional, uses sudo linux commands to create and add users to group. To be used on HDP Sandbox only.

@author: Jonathan Puvilland
'''
import json
import pkgutil
import argparse
from os import popen

GROUP_PREFIX='usr_prd_hdp_'
RESOURCES_PACKAGE='detinit.resources.typedefs'
SUBMIT_TO_OS = False

def read_cl_groups():
    ''' Reads Competition Law Atlas classifications and generates the corresponding linux group names.
        Return: a list of linux groups names
    '''
    groups = []
    resource_file = pkgutil.get_data(RESOURCES_PACKAGE, 'cl_classifications.json')
    classifications_defs = json.loads(resource_file.decode('utf-8'))
    if classifications_defs:
        classfications = classifications_defs['classificationDefs']
        for classfication in classfications:
            groups.append(GROUP_PREFIX + classfication['name'])
    else:
        print('Cannot load cl_classifications.json')
    return groups

def read_sg_groups():
    ''' Reads Security Governance Atlas classifications and generates the corresponding linux group names.
        Return: a list of linux groups names
    '''
    groups = []
    resource_file = pkgutil.get_data(RESOURCES_PACKAGE, 'sg_classifications.json')
    classifications_defs = json.loads(resource_file.decode('utf-8'))
    if classifications_defs:
        classfications = classifications_defs['classificationDefs']
        for classfication in classfications:
            groups.append(GROUP_PREFIX + classfication['name'])
    else:
        print('Cannot load sg_classifications.json')
    return groups

def read_pl_groups():
    ''' Reads Security Governance Atlas classifications and generates the corresponding linux group names.
        Return: a list of linux groups names
    '''
    groups = []
    resource_file = pkgutil.get_data(RESOURCES_PACKAGE, 'pl_classifications.json')
    classifications_defs = json.loads(resource_file.decode('utf-8'))
    if classifications_defs:
        classfications = classifications_defs['classificationDefs']
        for classfication in classfications:
            groups.append(GROUP_PREFIX + classfication['name'])
    else:
        print('Cannot load pl_classifications.json')
    return groups

def read_en_groups():
    ''' Reads Environments Atlas classifications and generates the corresponding linux group names.
        Return: a list of linux groups names

        For environments classification, two groups are created:
        - one for getting read only access (suffixed  with _RO)
        - one for getting read and write access (suffixed  with _RW)
    '''
    groups = []
    resource_file = pkgutil.get_data(RESOURCES_PACKAGE, 'en_classifications.json')
    classifications_defs = json.loads(resource_file.decode('utf-8'))
    if classifications_defs:
        classfications = classifications_defs['classificationDefs']
        for classfication in classfications:
            groups.append(GROUP_PREFIX + classfication['name'] + '_RO')
            groups.append(GROUP_PREFIX + classfication['name'] + '_RW')
    else:
        print('Cannot load en_classifications.json')
    return groups

def generate_linux_groups():
    ''' Creates a list of group names to be created on Linux. The linux group names have to be in lowercase.
    '''
    groups = []
    groups.extend(read_cl_groups())
    groups.extend(read_sg_groups())
    groups.extend(read_pl_groups())
    groups.extend(read_en_groups())
    return [group.lower() for group in groups]
    
def create_linux_groups(groups):
    ''' Creates the linux groups.
    '''
    for group in groups:
        print('adding group ' + group)
        
        if SUBMIT_TO_OS:
            popen('sudo groupadd -f ' + group, 'r')

def configure_user_raj(groups):
    user_groups = [group for group in groups if group.endswith('rw')]
    user_groups.extend([group for group in groups if group.startswith(GROUP_PREFIX + 'pl')])
    user_groups.extend([GROUP_PREFIX + 'cl_wr'])
    user_groups.extend([GROUP_PREFIX + 'sg_hiconf'])
    add_user_in_groups('raj_ops', user_groups)

def configure_user_amy(groups):
    user_groups = [group for group in groups if group.endswith('ro')]
    user_groups.extend([GROUP_PREFIX + 'en_ppads_dev_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_ppads_tst_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_ppads_acc_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_ppads_prd_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_sdb_irf_rw'])

    user_groups.extend([GROUP_PREFIX + 'pl_billing'])
    user_groups.extend([GROUP_PREFIX + 'pl_consumh'])
    user_groups.extend([GROUP_PREFIX + 'pl_interid'])

    user_groups.extend([GROUP_PREFIX + 'cl_rt'])
    user_groups.extend([GROUP_PREFIX + 'sg_conf'])
    add_user_in_groups('amy_ds', user_groups)

def configure_user_maria(groups):
    user_groups = [group for group in groups if group.endswith('ro')]
    user_groups.extend([GROUP_PREFIX + 'en_dwh_dev_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_dwh_tst_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_raw_dev_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_raw_tst_rw'])

    user_groups.extend([GROUP_PREFIX + 'pl_billing'])
    user_groups.extend([GROUP_PREFIX + 'pl_interid'])
    user_groups.extend([GROUP_PREFIX + 'pl_subscri'])
    user_groups.extend([GROUP_PREFIX + 'pl_trfdata'])

    user_groups.extend([GROUP_PREFIX + 'cl_wr'])
    user_groups.extend([GROUP_PREFIX + 'sg_conf'])
    add_user_in_groups('maria_dev', user_groups)

def configure_user_holger():
    user_groups = []
    user_groups.extend([GROUP_PREFIX + 'en_euds_dev_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_euds_tst_rw'])
    user_groups.extend([GROUP_PREFIX + 'en_euds_acc_ro'])
    user_groups.extend([GROUP_PREFIX + 'en_euds_prd_ro'])
    user_groups.extend([GROUP_PREFIX + 'en_dwh_dev_ro'])
    user_groups.extend([GROUP_PREFIX + 'en_dwh_tst_ro'])
    user_groups.extend([GROUP_PREFIX + 'en_dwh_acc_ro'])
    user_groups.extend([GROUP_PREFIX + 'en_dwh_prd_ro'])

    user_groups.extend([GROUP_PREFIX + 'pl_billing'])
    user_groups.extend([GROUP_PREFIX + 'pl_consumh'])
    user_groups.extend([GROUP_PREFIX + 'pl_subscri'])
    user_groups.extend([GROUP_PREFIX + 'pl_trfdata'])

    user_groups.extend([GROUP_PREFIX + 'cl_wr'])
    user_groups.extend([GROUP_PREFIX + 'sg_conf'])
    add_user_in_groups('holger_gov', user_groups)

def configure_user_dummy(groups):
    ''' Create a dummy linux user member of all groups used by Atlas Tag based policy
        This is necessary to create Tag based policies in Ranger that do not import linux empty groups
    '''
    user = 'dummy'
    if SUBMIT_TO_OS:
        popen('sudo useradd {0}'.format(user), 'r')
    add_user_in_groups(user, groups)

def add_user_in_groups(user, user_groups):
    # Resetting default group
    if SUBMIT_TO_OS:
        popen('sudo usermod -G {0} {0}'.format(user), 'r')

    for group in user_groups:
        print('adding user {0} in {1}'.format(user, group))
        if SUBMIT_TO_OS:
            popen('sudo usermod -a -G {1} {0}'.format(user, group), 'r')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='--submit creates the linux groups on the server.')

    parser.add_argument('--submit', dest='submit', action='store_true')
    parser.set_defaults(submit=False)
    args = parser.parse_args()
    
    SUBMIT_TO_OS = args.submit

    groups = generate_linux_groups()
    create_linux_groups(groups)
    configure_user_dummy(groups)
    configure_user_raj(groups)
    configure_user_amy(groups)
    configure_user_maria(groups)
    configure_user_holger()
