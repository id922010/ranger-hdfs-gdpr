import unittest
import requests
import json
import time
from det.settings import DET_WEBSERVER_ADDRESS
from det.settings import DET_WEBSERVER_PORT
from det.app import DET_API
from credentials import credentials
from urllib import parse

RESOURCES = './tests/resources/entities/'
HDFSPATH_RESOURCE = 'entities/hdfs_paths'

class TestDETHDFSPath(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.creds = credentials.require(['DET_API_TOKEN', 'CLUSTER'])
        
        cls.DET_BASE_URL = parse.urljoin("http://{host}:{port}". \
                                format(host = DET_WEBSERVER_ADDRESS, port = DET_WEBSERVER_PORT), \
                                DET_API.base_path + '/')
        cls.DET_HEADER = {'Content-Type': "application/json", 'x-api-key': cls.creds.DET_API_TOKEN}
        cls.EPOCH_PATH = str(time.time())
        cls.DET_UNITTEST_PATH = {
            "data_code": "data",
            "env": "d0",
            "app": "raw",
            "folder": "det_unittest",
            "classifications": {
                "Retainable": {
                    "retentionPeriod": 30
                },
                "cl": "CL_WR",
                "pl": ["PL_NOTAPPL"],
                "sg": "SG_IUO"
            }
        }

        # Creates the det_unittest path if not exists
        url = parse.urljoin(cls.DET_BASE_URL, HDFSPATH_RESOURCE)
        requests.post(url, data=json.dumps(cls.DET_UNITTEST_PATH), headers=cls.DET_HEADER)

    def test_get_200(self):
        ''' Integration testing: test_get_200
            Validates DET can successfully get a hdfs_path entity
            Assumes hdfs_path /data/d0/raw/cdb exists
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
        payload = {'qualifiedName': '/data/d0/raw/det_unittest'}
        response = requests.get(url, params = payload)
        self.assertEqual(response.status_code, 200)

    def test_post_201(self):
        ''' Integration testing: test_post_201
            Validates DET can create a new path (random generated)
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
    
        # Creates a new random hdfs_path subfolder
        hdfs_path = self.DET_UNITTEST_PATH.copy()
        hdfs_path["subfolder"] = self.EPOCH_PATH
        response = requests.post(url, data=json.dumps(hdfs_path), headers=self.DET_HEADER)
        self.assertEqual(response.status_code, 201)

    def test_post_400(self):
        ''' Integration testing: test_post_400
            Bad JSON Content
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
    
        # Remove a mandatory attribute
        hdfs_path = self.DET_UNITTEST_PATH.copy()
        del hdfs_path["app"]
        response = requests.post(url, data=json.dumps(hdfs_path), headers=self.DET_HEADER)
        self.assertEqual(response.status_code, 400)

    def test_post_401(self):
        ''' Integration testing: test_post_401
            Required TOKEN in header
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
        
        # Remove token from HEADER
        unsave_header = self.DET_HEADER.copy()
        del unsave_header["x-api-key"]
        response = requests.post(url, data=json.dumps(self.DET_UNITTEST_PATH), headers=unsave_header)
        self.assertEqual(response.status_code, 401)

    def test_post_403(self):
        ''' Integration testing: test_post_403
            Cannot update an existing hdfs_path with POST
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
        response = requests.post(url, data=json.dumps(self.DET_UNITTEST_PATH), headers=self.DET_HEADER)
        self.assertEqual(response.status_code, 403)

    def test_put_200(self):
        ''' Integration testing: test_put_200
            Validates updates on an existing path returns 200 and updated fields
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
        
        # Create first a new hdfs_path with default classifications
        hdfs_path = self.DET_UNITTEST_PATH
        hdfs_path["subfolder"] = self.EPOCH_PATH
        requests.post(url, data=json.dumps(hdfs_path), headers=self.DET_HEADER)

        # Change classifications and perform update
        hdfs_path["classifications"]["cl"] = "CL_WR"
        hdfs_path["classifications"]["pl"] = ["PL_3RDPART", "PL_FINANCE"]
        hdfs_path["classifications"]["sg"] = "SG_CONF"
        response = requests.put(url, data=json.dumps(hdfs_path), headers=self.DET_HEADER)
        self.assertEqual(response.status_code, 200)

        # Validates updated attributes
        payload = {'qualifiedName': '/data/d0/raw/det_unittest/{}'.format(self.EPOCH_PATH)}
        response = requests.get(url, params = payload).json()
        classifications = []
        for classification in response['classifications']:
            classifications.append(classification["typeName"])
        self.assertIn(hdfs_path["classifications"]["cl"], classifications)
        self.assertIn(hdfs_path["classifications"]["sg"], classifications)
        for pl_classification in hdfs_path["classifications"]["pl"]:
            self.assertIn(pl_classification, classifications)

    def test_put_400(self):
        ''' Integration testing: test_put_400
            Bad JSON Content
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
    
        # Remove a mandatory attribute
        hdfs_path = self.DET_UNITTEST_PATH.copy()
        del hdfs_path["app"]
        response = requests.put(url, data=json.dumps(hdfs_path), headers=self.DET_HEADER)
        self.assertEqual(response.status_code, 400)

    def test_put_401(self):
        ''' Integration testing: test_put_401
            Required TOKEN in header
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
        
        # Remove token from HEADER
        unsave_header = self.DET_HEADER.copy()
        del unsave_header["x-api-key"]
        response = requests.put(url, data=json.dumps(self.DET_UNITTEST_PATH), headers=unsave_header)
        self.assertEqual(response.status_code, 401)

    def test_put_404(self):
        ''' Integration testing: test_put_404
            Validates updates on a missing path returns 404 NOT FOUND
        '''
        url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
        hdfs_path = self.DET_UNITTEST_PATH
        hdfs_path["subfolder"] = str(time.time())

        response = requests.put(url, data=json.dumps(hdfs_path), headers=self.DET_HEADER)
        self.assertEqual(response.status_code, 404)

    def test_post_detinit_defaults(self):
        ''' Integration testing: test_post_201_403
            Validates DET can create successfully various hdfs_path entities or that
            existing path returns a 403 FORBIDDEN
        '''
        with open(RESOURCES + "hdfs_paths.json") as json_file:
            url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
            hdfs_paths = json.load(json_file)
                   
            for hdfs_path in hdfs_paths:
                payload = json.dumps(hdfs_path)
                response = requests.post(url, data=payload, headers=self.DET_HEADER)
                
                self.assertIn(response.status_code, [201, 403])

    def test_post_sdb_403(self):
        ''' Integration testing: test_post_sdb_403
            Validates sandbox application have s0 has mandatory environments
             
        '''
        with open (RESOURCES +'hdfs_paths_sandbox.json') as json_file:
            url = parse.urljoin(self.DET_BASE_URL, HDFSPATH_RESOURCE)
            hdfs_paths = json.load(json_file)
            
            for hdfs_path in hdfs_paths:
                payload = json.dumps(hdfs_path)
                response = requests.post(url, data=payload, headers=self.DET_HEADER)
                self.assertEqual(response.status_code, 403)

if __name__ == '__main__':
    unittest.main()
