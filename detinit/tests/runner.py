import unittest

# import test modules
from detinit.tests import test_det_admin
from detinit.tests import test_det_cluster
from detinit.tests import test_det_hdfspath
from detinit.tests import test_det_typedefs

def intergration_test_suite():
    # initialize the test suite
    loader = unittest.TestLoader()
    suite  = unittest.TestSuite()
    
    # add tests to the test suite
    suite.addTests(loader.loadTestsFromModule(test_det_admin))
    suite.addTests(loader.loadTestsFromModule(test_det_cluster))
    suite.addTests(loader.loadTestsFromModule(test_det_hdfspath))
    suite.addTests(loader.loadTestsFromModule(test_det_typedefs))
    
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(intergration_test_suite())
