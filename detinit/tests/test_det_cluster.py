import unittest
import requests
from det.settings import DET_WEBSERVER_ADDRESS
from det.settings import DET_WEBSERVER_PORT
from det.app import DET_API
from credentials import credentials
from urllib import parse

class TestDETCluster(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.creds = credentials.require(['DET_API_TOKEN', 'CLUSTER'])
        
        cls.DET_BASE_URL = parse.urljoin("http://{host}:{port}". \
                                format(host = DET_WEBSERVER_ADDRESS, port = DET_WEBSERVER_PORT), \
                                DET_API.base_path + '/')
        cls.DET_HEADER = {'Content-Type': "application/json", 'x-api-key': cls.creds.DET_API_TOKEN}

    def test_det_clusters(self):
        ''' Integration testing: test_det_clusters
            Validates the HDP cluster name
        '''
        url = parse.urljoin(self.DET_BASE_URL, 'clusters')
        response = requests.get(url).json()
        self.assertEqual(response[0], self.creds.CLUSTER)

    def test_det_cluster_name_200(self):
        ''' Integration testing: test_det_cluster_name_200
            cluster_name is a valid HDP Cluster as specified in the credentials file
        '''
        url = parse.urljoin(self.DET_BASE_URL, 'clusters/{}'.format(self.creds.CLUSTER))
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

    def test_det_cluster_name_404(self):
        ''' Integration testing: test_det_cluster_name_404
            cluster_name is not a valid HDP Cluster
        '''
        url = parse.urljoin(self.DET_BASE_URL, 'clusters/this_is_a_fake_cluster_name')
        response = requests.get(url)
        self.assertEqual(response.status_code, 404)
    
    def test_det_cluster_version_2_6(self):
        ''' Integration testing: test_det_cluster_version_2_6
            Validates the HDP cluster version
        '''
        url = parse.urljoin(self.DET_BASE_URL, 'clusters/{}'.format(self.creds.CLUSTER))
        response = requests.get(url).json()
        self.assertEqual(response['version'], 'HDP-2.6')

    def test_det_cluster_services_200(self):
        ''' Integration testing: test_det_cluster_services_200
            cluster_name is a valid HDP Cluster as specified in the credentials file
        '''
        url = parse.urljoin(self.DET_BASE_URL, 'clusters/{}/services'.format(self.creds.CLUSTER))
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

    def test_det_cluster_services_404(self):
        ''' Integration testing: test_det_cluster_services_404
            cluster_name is not a valid HDP Cluster
        '''
        url = parse.urljoin(self.DET_BASE_URL, 'clusters/this_is_a_fake_cluster_name/services')
        response = requests.get(url)
        self.assertEqual(response.status_code, 404)

    def test_det_cluster_services_started(self):
        ''' Integration testing: test_det_cluster_services_started
            Validates the required HDP services are started
        '''
        required_services = ['AMBARI_INFRA', 'ATLAS', 'HBASE', 'KAFKA', 'RANGER']
        url = parse.urljoin(self.DET_BASE_URL, 'clusters/{}/services'.format(self.creds.CLUSTER))
        response = requests.get(url).json()
    
        for service in response:
            if service['service_name'] in required_services:
                self.assertEqual(service['state'], 'STARTED')

if __name__ == '__main__':
    unittest.main()
