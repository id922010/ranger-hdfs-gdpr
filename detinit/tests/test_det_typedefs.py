import unittest
import requests
import pkgutil
import json
from det.settings import DET_WEBSERVER_ADDRESS
from det.settings import DET_WEBSERVER_PORT
from det.app import DET_API
from credentials import credentials
from urllib import parse

RESOURCES='./tests/resources/typedefs/'
RESOURCES_PACKAGE='detinit.resources.typedefs'
TYPEDEFS_RESOURCE = 'typedefs'
ENUMDEFS_RESOURCE = TYPEDEFS_RESOURCE + '/enumdefs'
CLASSIFICATIONDEFS_RESOURCE = TYPEDEFS_RESOURCE + '/classificationdefs'
ENTITYDEFS_RESOURCE = TYPEDEFS_RESOURCE + '/entitydefs'

def setUpModule():
    global DET_HEADER, DET_BASE_URL

    creds = credentials.require(['DET_API_ADMIN_TOKEN', 'CLUSTER'])
    DET_HEADER = {'Content-Type': "application/json", 'x-api-key': creds.DET_API_ADMIN_TOKEN}
    DET_BASE_URL = parse.urljoin("http://{host}:{port}". \
                            format(host = DET_WEBSERVER_ADDRESS, port = DET_WEBSERVER_PORT), \
                            DET_API.base_path + '/')
class TestDETTypeDefsEnum(unittest.TestCase):
    
    def test_det_enum_get(self):
        ''' Integration testing: test_det_enum_get
            Validates all required enum definitions are defined on the cluster 
        '''
        url = parse.urljoin(DET_BASE_URL, ENUMDEFS_RESOURCE)
        response = requests.get(url).json()
    
        required_enums = ['hive_principal_type', 'file_action']
    
        for enum in response:
            try:
                required_enums.remove(enum['name'])
            except ValueError:
                pass
        
        self.assertEqual(len(required_enums), 0)
    
    def test_det_enum_post(self):
        ''' Integration testing: test_det_enum_post
            Validates DET can create successfully an enum type definition 
        '''
        with open(RESOURCES + 'enum.json') as file_handle:
            enum_typedef = file_handle.read()
            url = parse.urljoin(DET_BASE_URL, TYPEDEFS_RESOURCE)
            response = requests.request("POST", url, data=enum_typedef, headers=DET_HEADER)
        
            self.assertIn(response.status_code, [201, 403])

class TestDETTypeDefsClassification(unittest.TestCase):

    def classification_defs(self):
        ''' Generates a list of the required classifications for DET initialization
        '''
        classification_files = ['cl_classifications.json',
                                'en_classifications.json',
                                'pl_classifications.json',
                                'sg_classifications.json',
                                'retainable_classification.json']
        defs = []
        for classification_file in classification_files:
            defs.extend(self.read_classification_file(classification_file))
        return defs
    
    def read_classification_file(self, filename):
        ''' Reads a classification definition file from the DET init resources folder
            and extracts the name of the classification 
        '''
    
        defs = []
        resource_file = pkgutil.get_data(RESOURCES_PACKAGE, filename)
        classifications_defs = json.loads(resource_file.decode('utf-8'))
        if classifications_defs:
            classfications = classifications_defs['classificationDefs']
            for classfication in classfications:
                defs.append(classfication['name'])
        return defs

    def test_det_classification_get(self):
        ''' Integration testing: test_det_classification_get
            Validates all required classification definitions are defined on the cluster 
        '''
        url = parse.urljoin(DET_BASE_URL, CLASSIFICATIONDEFS_RESOURCE)
        response = requests.get(url).json()
    
        classification_defs = self.classification_defs()
        for classification in response:
            try:
                classification_defs.remove(classification['name'])
            except ValueError:
                pass
        
        self.assertEqual(len(classification_defs), 0)
    
    def test_det_classification_post(self):
        ''' Integration testing: test_det_classification_post
            Validates DET can create successfully a classification type definition
            - a basic classification
            - a classification with attributes 
        '''
        with open(RESOURCES + 'classification.json') as file_handle:
            classification_typedef = file_handle.read()
            url = parse.urljoin(DET_BASE_URL, TYPEDEFS_RESOURCE)
            response = requests.request("POST", url, data=classification_typedef, headers=DET_HEADER)
        
            self.assertIn(response.status_code, [201, 403])

class TestDETTypeDefsEntity(unittest.TestCase):

    def test_det_entity_get(self):
        ''' Integration testing: test_det_entity_get
            Validates all required entity definitions are defined on the cluster 
        '''
        url = parse.urljoin(DET_BASE_URL, ENTITYDEFS_RESOURCE)
        response = requests.get(url).json()
    
        required_entity_defs = ['hdfs_path', 'fs_path', 'DataSet']
    
        for entity_def in response:
            try:
                required_entity_defs.remove(entity_def['name'])
            except ValueError:
                pass

        self.assertEqual(len(required_entity_defs), 0)
    
    def test_det_entity_post(self):
        ''' Integration testing: test_det_entity_post
            Validates DET can create successfully an entity type definition with attributes
        '''
        with open(RESOURCES + 'entity.json') as file_handle:
            entity_typedef = file_handle.read()
            url = parse.urljoin(DET_BASE_URL, TYPEDEFS_RESOURCE)
            response = requests.post(url, data=entity_typedef, headers=DET_HEADER)
        
            self.assertIn(response.status_code, [201, 403])

class TestDETTypeDefs(unittest.TestCase):
    
    def test_det_typedef_post_401(self):
        ''' Integration testing: test_det_typedef_post_401
            Required TOKEN in header
        '''
        with open(RESOURCES + 'entity.json') as file_handle:
            entity_typedef = file_handle.read()
            url = parse.urljoin(DET_BASE_URL, TYPEDEFS_RESOURCE)
        
            # Remove token from HEADER
            unsave_header = DET_HEADER.copy()
            del unsave_header["x-api-key"]
            response = requests.post(url, data=entity_typedef, headers=unsave_header)
            self.assertEqual(response.status_code, 401)

    def test_det_typedef_post_400(self):
        ''' Integration testing: test_det_typedef_post_400
            Invalid JSON content
        '''
        entity_typedef = dict()
        entity_typedef["entityDefs"] = []
        url = parse.urljoin(DET_BASE_URL, TYPEDEFS_RESOURCE)
        
        response = requests.post(url, data=entity_typedef, headers=DET_HEADER)
        self.assertEqual(response.status_code, 400)

if __name__ == '__main__':
    unittest.main()
