import unittest
import requests
from det.settings import DET_WEBSERVER_ADDRESS
from det.settings import DET_WEBSERVER_PORT
from det.app import DET_API
from credentials import credentials
from det import __version__
from urllib import parse

class TestDETAdmin(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.creds = credentials.require(['DET_API_TOKEN', 'CLUSTER'])
        
        cls.DET_BASE_URL = parse.urljoin("http://{host}:{port}". \
                                format(host = DET_WEBSERVER_ADDRESS, port = DET_WEBSERVER_PORT), \
                                DET_API.base_path + '/')
        cls.DET_HEADER = {'Content-Type': "application/json", 'x-api-key': cls.creds.DET_API_TOKEN}

    def test_det_version(self):
        ''' Integration testing: test_det_version
            Validates the DET version number
        '''
        url = parse.urljoin(self.DET_BASE_URL, 'swagger.json')
        response = requests.get(url).json()
        self.assertEqual(response['info']['version'], __version__)

    def test_det_status_200(self):
        ''' Integration testing: test_det_status_ok
            Validates the DET API is alive
        '''
        url = parse.urljoin(self.DET_BASE_URL, 'swagger.json')
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()