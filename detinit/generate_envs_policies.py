'''
This script generates Ranger policy JSON files related to BDP environments.

parameters:
  - cluster: the name of the target cluster where the policies will be deployed. One of [TDLKD, TDLKP, SANDBOX]
  - ranger_service: the name of the Ranger's Service for holding the tags policies

Created on 20 mai 2018
@author: Jonathan Puvilland
'''
import json
import argparse

ENVS = ['dev', 'tst', 'acc', 'prd']
CONDITION_EXP = "if (ctx.getUserGroups().contains(\"${group_name}_ro\"))\n\tctx.result = false;\nelse if (ctx.getUserGroups().contains(\"${group_name}_rw\"))\n\tctx.result = false;\nelse\n\tctx.result = true;"

def update_default_policy(policy, app_name, env_name, group_prefix):
    ''' Updates the default policy template with the application environment.
        
        Input:
        - policy: a json dictionary with the policy definition
        - app_name: the name of the application (ex. DWH, UXMT)
        - env_name: the name of the environment (ex. dev, tst)
        - group_prefix: the linux group prefix used to assign group level permission in Ranger

        Output
        - policy definition
    '''
    app_env = ('EN_' + app_name + '_' + env_name).upper()
    rule_condition = CONDITION_EXP.replace('${group_name}', group_prefix + app_env.lower())
    policy["name"] = app_env
    policy["resources"]["tag"]["values"][0] = app_env
    policy["policyItems"][0]["groups"][0] = group_prefix + app_env.lower() + "_ro"
    policy["policyItems"][1]["groups"][0] = group_prefix + app_env.lower() + "_rw"
    policy["denyPolicyItems"][0]["conditions"][0]["values"][0] = rule_condition 

def save_policy(policy):
    ''' Saves the policy to disk in a temporary folder

        Input:
        - policy: a json dictionary with the policy definition
    '''
    # print(json.dumps(policy, indent=2))
    file_name = './resources/tmp/tag_' + policy["name"].lower() + '.json'
    print('Saving policy file {0}'.format(file_name))
    with open(file_name, 'w') as fp:
        json.dump(policy, fp, indent=2)

def generate_app_policy(app_name, service_name='Sandbox_tag', group_prefix='usr_prd_hdp'):
    ''' Generates all ranger policies for an application in all environments.
        Reads the default policy template and saves generated ranger policies.

        Input:
        - app_name: the name of the application (ex. DWH, UXMT)
        - service_name: the Ranger service name hosting the policies
        - group_prefix: the linux group prefix used to assign group level permission in Ranger
    '''
    with open('./resources/ranger/en_policy_template.json') as file_handle:
        policy = json.load(file_handle)
        policy["service"] = service_name
        
        for env in ENVS:
            update_default_policy(policy, app_name, env, group_prefix)
            save_policy(policy)

if __name__ == '__main__':
    clusters = {
            "TDLKP": "usr_prd_hdp_",
            "TDLKD": "usr_dev_hdp_",
            "SANDBOX": "usr_prd_hdp_"
        }  
    parser = argparse.ArgumentParser(description='Provide target cluster and ranger service.')
    parser.add_argument('-c', '--cluster', dest='cluster', choices=clusters.keys(), action='store')
    parser.add_argument('-s', '--service', dest='ranger_service', type=str, action='store')
    args = parser.parse_args()

    apps = ['RAW', 'DWH', 'EUDS', 'PPADS', 'UXMT']
    for app in apps:
        generate_app_policy(app, service_name=args.ranger_service, group_prefix=clusters[args.cluster])
