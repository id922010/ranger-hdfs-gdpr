'''
This script generates Ranger policy JSON files related to BDP privacy law tags.

parameters:
  - cluster: the name of the target cluster where the policies will be deployed. One of [TDLKD, TDLKP, SANDBOX]
  
Created on 20 mai 2018
@author: Jonathan Puvilland
'''
import json
import argparse

CONDITION_EXP = "if (ctx.getUserGroups().contains(\"${group_name}\"))\n\tctx.result \u003d false;\nelse\n\tctx.result \u003d true;"

def update_default_policy(policy, pl_classification, group_prefix):
    ''' Updates the default policy template with the Privacy Law classification details.
        
        Input:
        - policy: a json dictionary with the policy definition
        - pl_classification: a dictionary representing the Atlas Privacy Law tag definition
        - group_prefix: the linux group prefix used to assign group level permission in Ranger

        Output
        - updated policy
    '''
    rule_condition = CONDITION_EXP.replace('${group_name}', group_prefix + pl_classification["name"].lower())
    policy["name"] = pl_classification["name"]
    policy["description"] = "Read Any " + pl_classification["description"]
    policy["resources"]["tag"]["values"][0] = pl_classification["name"]
    policy["policyItems"][0]["groups"][0] = group_prefix + pl_classification["name"].lower()
    policy["denyPolicyItems"][0]["conditions"][0]["values"][0] = rule_condition  

def save_policy(policy):
    ''' Saves the policy to disk in a temporary folder

        Input:
        - policy: a json dictionary with the policy definition
    '''
    # print(json.dumps(policy, indent=2))
    file_name = './resources/tmp/tag_' + policy["name"].lower() + '.json'
    print('Saving policy file {0}'.format(file_name))
    with open(file_name, 'w') as fp:
        json.dump(policy, fp, indent=2)

def generate_pl_policies(service_name='Sandbox_tag', group_prefix='usr_prd_hdp'):
    ''' Generates all ranger policies for privacy law tags
        Reads the list of Privacy Law tags.
        Reads the default policy template and saves generated ranger policies.

        Input:
        - service_name: the Ranger service name hosting the policies
        - group_prefix: the linux group prefix used to assign group level permission in Ranger
        - group_prefix: the linux group prefix used to assign group level permission in Ranger
    '''
    with open('./resources/typedefs/pl_classifications.json') as file_handle:
        pl_classifications = json.load(file_handle)['classificationDefs']

    with open('./resources/ranger/pl_policy_template.json') as file_handle:
        policy = json.load(file_handle)
        policy["service"] = service_name

    for pl_classification in pl_classifications:
        update_default_policy(policy, pl_classification, group_prefix)
        save_policy(policy)

if __name__ == '__main__':
    clusters = {
            "TDLKP": "usr_prd_hdp_",
            "TDLKD": "usr_dev_hdp_",
            "SANDBOX": "usr_prd_hdp_"
        }  
    parser = argparse.ArgumentParser(description='Provide target cluster and ranger service.')
    parser.add_argument('-c', '--cluster', dest='cluster', choices=clusters.keys(), action='store')
    parser.add_argument('-s', '--service', dest='ranger_service', type=str, action='store')

    args = parser.parse_args()
    generate_pl_policies(service_name=args.ranger_service, group_prefix=clusters[args.cluster])
