'''
Created on 20 mai 2018
This scripts creates the HDFS home folders (/data) used by the DET. 
groups.

Usage:
- log in terminal with DET user
- Initiate kerberos ticket
- Execute main script

@author: Jonathan Puvilland
'''
import requests
from requests_kerberos import HTTPKerberosAuth
from det.app import conf

ENVS = ['d0', 't0', 'a7', 'p0']
APPS = ['raw', 'dwh', 'euds', 'ppads', 'uxmt']
WEBHDFS_BASE_URL = 'http://' + conf['WEBHDFS_HOST'] + ':' +  str(conf['WEBHDFS_PORT']) + '/webhdfs/v1'

if __name__ == '__main__':
    # Create environment and application root folders
    for env in ENVS:
        for app in APPS:
            path = '/data/{env}/{app}'.format(env=env, app=app).lower()
            querystring = {"op":"MKDIRS", "permission":"770"}
            url = WEBHDFS_BASE_URL + path
            response = requests.put(url, params=querystring, auth=HTTPKerberosAuth())
            print(path, response.status_code)
    
    # Create Sandboxes root folder
    path = '/data/s0/sdb'
    url = WEBHDFS_BASE_URL + path
    response = requests.put(url, params=querystring, auth=HTTPKerberosAuth())
    print(path, response.status_code)
    
    """ to do: use Atlas bulk API to add all root envs hdfs_paths
        http://localhost:21000/api/atlas/v2/entity/bulk
        bulkfile: detinit/resources/entities/hdfs_path_root_envs.json
    """