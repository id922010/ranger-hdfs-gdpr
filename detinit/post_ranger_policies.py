'''
Created on 20 mai 2018

@author: Jonathan Puvilland

This script uses the Ranger REST API to post hdfs and tag based policies to a local mapped cluster
'''
import requests
import glob

URL = "http://localhost:6080/service/public/v2/api/policy"
HEADERS = {
        'Content-Type': "application/json",
        'Authorization': "Basic cmFqX29wczpyYWpfb3Bz",
        'Cache-Control': "no-cache",
        'Postman-Token': "e9baf8c3-2ab2-881d-73de-d32f1b7e6f57"
    }
RESOURCES = [
   './resources/ranger/hdfs/*.json',
   './resources/ranger/tags_envs/*.json',
    './resources/ranger/tags_sg/*.json',
    './resources/ranger/tags_cl/*.json',
    './resources/ranger/tags_pl/*.json'
    ]

if __name__ == '__main__':
    policy_files = []
    for resource in RESOURCES:
        policy_files.extend(glob.glob(resource))

    print('Creating {0} policies'.format(len(policy_files)))

    for policy_file in policy_files:
        with open(policy_file) as payload:
            print('Creating policy {0} ...'.format(policy_file))
            response = requests.request("POST", URL, data=payload.read(), headers=HEADERS)
            if response.status_code == 400 and response.text.find('Another policy already exists') >= 0:
                print('Policy already exists.')
            elif response.status_code != 200:
                print(response.text)
            else:
                print('Policy successfully created.')
